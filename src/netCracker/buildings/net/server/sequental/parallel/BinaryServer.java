package netCracker.buildings.net.server.sequental.parallel;

import netCracker.Building;
import netCracker.Buildings;
import netCracker.buildings.Dwellings.DwellingFactory;
import netCracker.buildings.Dwellings.Hotel.HotelFactory;
import netCracker.buildings.Offices.OfficeFactory;
import netCracker.buildings.net.BuildingUnderArrestException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

/**
 * Created by Alexey on 30.11.2016.
 */
public class BinaryServer {
    private static synchronized double costOfBuilding(Building building) throws BuildingUnderArrestException {
        if (new Random().nextInt(10) == 0) {
            throw new BuildingUnderArrestException();
        } else {
            switch (building.getClass().getSimpleName()) {
                case "OfficeBuilding":
                    return building.getArea() * 1500;
                case "Dwelling":
                    return building.getArea() * 1000;
                case "Hotel":
                    return building.getArea() * 2000;
                default:
                    return 0;
            }
        }
    }

    public static void main(String[] args) throws IOException {

        System.out.println("Сервер ждет подключений...");
        ServerSocket serverSocket = new ServerSocket(51221);
        while (true) {
            Socket socket = serverSocket.accept();
            Thread th = new Thread(() -> {
                try {

                    System.out.println("Сервер установил соединение...");
                    double price = 0;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    DataInputStream dataIn = new DataInputStream(socket.getInputStream());
                    String buffer;
                    Building building;

                    try {
                        while (!(buffer = dataIn.readUTF()).equals("stop")) {
                            System.out.println("Получен тип здания: " + buffer);
                            switch (buffer) {
                                case "Dwelling":
                                    Buildings.setBuildingFactory(new DwellingFactory());
                                    break;
                                case "OfficeBuilding":
                                    Buildings.setBuildingFactory(new OfficeFactory());
                                    break;
                                case "Hotel":
                                    Buildings.setBuildingFactory(new HotelFactory());
                                    break;
                            }

                            building = Buildings.inputBuilding(socket.getInputStream());
                            System.out.println("Получено здание: " + building);
                            try {
                                System.out.println("Идет оценка стоимости здания...");
                                price = costOfBuilding(building);
                            } catch (BuildingUnderArrestException e) {
                                //e.printStackTrace();
                                bufferedWriter.write("Здание под арестом." + "\n");
                                bufferedWriter.flush();
                                System.out.println("Здание под арестом.");

                            }
                            bufferedWriter.write(String.valueOf(price) + "$" + "\n");
                            bufferedWriter.flush();
                            System.out.println("Отправлена стоимость здания: " + price);
                            System.out.println();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            th.start();
        }
    }
}

