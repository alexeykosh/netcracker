package netCracker.buildings.Threads;

/**
 * Created by Alexey on 28.11.2016.
 */
public class Semaphore {
    private boolean flag;
    private Thread thread;

    public Semaphore() {
        flag = true;
    }

    public void setLastThread(Thread thread) {
        this.thread = thread;
    }

    public synchronized void acquire() throws InterruptedException {
        if (Thread.currentThread() != thread) {
            while (!flag) {
                this.wait();
            }
            flag = false;

        } else this.wait();
    }

    public synchronized void release() {
        flag = true;
        thread = Thread.currentThread();
        this.notify();

    }
}
