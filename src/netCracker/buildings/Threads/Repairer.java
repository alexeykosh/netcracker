package netCracker.buildings.Threads;

import netCracker.Floor;

/**
 * Created by Home on 15.11.2016.
 */
public class Repairer extends Thread {
    private Floor floor;

    public Repairer(Floor floor) {
        this.floor = floor;
    }

    @Override
    public void run() {
        for (int i = 0; i < floor.getNumberOfSpaces(); i++) {
            System.out.println("Repairing space number " + i + " with total area " +
                    floor.getSpace(i).getArea() + " square meters».");
        }
    }
}
