package netCracker.buildings.Dwellings;


import netCracker.PlacementExchanger;
import netCracker.Space;

import java.io.Serializable;

/**
 * Created by Aleksey on 04.10.2016.
 */

public class Flat implements Space, Serializable, Cloneable {
    private static final int NUMBER_OF_ROOMS = 2;
    private static final double SQUARE = 50;

    private int numberOfRooms;
    private double square;

    public Flat() {
        this.numberOfRooms = NUMBER_OF_ROOMS;
        this.square = SQUARE;
    }

    public Flat(double square) {
        this.square = square;
        numberOfRooms = NUMBER_OF_ROOMS;
    }

    public Flat(int numberOfRooms, double square) {
        this.square = square;
        this.numberOfRooms = numberOfRooms;
    }

    @Override
    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    @Override
    public void setRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    @Override
    public double getArea() {
        return square;
    }

    @Override
    public void setArea(double square) {
    }

    @Override
    public String toString() {
        return "Flat " + "(" + numberOfRooms + ", " + square + ")";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(obj instanceof Flat)) return false;
        return PlacementExchanger.isExchangeSpace(this, (Flat) obj);
    }

    @Override
    public int hashCode() {
        int hash;
        Double area = getArea();
        long l = area.longValue();
        hash = numberOfRooms ^ ((int) l ^ (int) l >> 32);
        return hash;
    }

    @Override
    public int compareTo(Space o) {
        if (o.getArea() == this.getArea()) return 0;
        if (o.getArea() > this.getArea()) return -1;
        return 1;
        //вернет -1, если О больше,чем этот объект.
    }
}
