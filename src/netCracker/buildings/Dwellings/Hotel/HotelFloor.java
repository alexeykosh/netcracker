package netCracker.buildings.Dwellings.Hotel;

import netCracker.PlacementExchanger;
import netCracker.Space;
import netCracker.buildings.Dwellings.DwellingFloor;

/**
 * Created by Alexey on 24.10.2016.
 */
public class HotelFloor extends DwellingFloor {
    private static final String STARS = "*";
    private String stars;

    public HotelFloor(int numberOfFlats) {
        super(numberOfFlats);
        stars = STARS;
    }

    public HotelFloor(Space[] space) {
        super(space);
        stars = STARS;
    }

    public void setStars(int stars) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < stars; i++) {
            sb.append("*");
        }
        this.stars = sb.toString();
    }

    public String getStars() {
        return stars;
    }

    @Override
    public int getNumberOfSpaces() {
        return super.getNumberOfSpaces();
    }

    @Override
    public double getArea() {
        return super.getArea();
    }

    @Override
    public int getNumberOfRooms() {
        return super.getNumberOfRooms();
    }

    @Override
    public Space[] getArrayOfSpaces() {
        return super.getArrayOfSpaces();
    }

    @Override
    public Space getSpace(int numberOfFlat) {
        return super.getSpace(numberOfFlat);
    }

    @Override
    public void setSpace(int numberOfFlat, Space space) {
        super.setSpace(numberOfFlat, space);
    }

    @Override
    public void addNewSpace(int numberOfFlat, Space flat) {
        super.addNewSpace(numberOfFlat, flat);
    }

    @Override
    public void deleteSpace(int numberOfFlat) {
        super.deleteSpace(numberOfFlat);
    }

    @Override
    public Space getBestSpace() {
        return super.getBestSpace();
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < super.getNumberOfSpaces(); i++) {
            stringBuffer.append(", Flat (" + super.getSpace(i).getNumberOfRooms() + ", " + super.getSpace(i).getArea() + ")");
        }
        return "Hotel Floor (" + super.getNumberOfSpaces() + stringBuffer + getStars() + ")";

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof HotelFloor)) return false;
        return PlacementExchanger.isExchangeFloor(this, (HotelFloor) obj);
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfSpaces();
        for (int i = 0; i < this.getNumberOfSpaces(); i++) {
            hash = hash ^ this.getSpace(i).hashCode();
        }
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); // тут вопрос!!!!
    }

}
