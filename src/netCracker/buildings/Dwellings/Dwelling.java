package netCracker.buildings.Dwellings;

import netCracker.*;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Created by Aleksey on 04.10.2016.
 */
public class Dwelling implements Building, Serializable, Iterable {
    private Floor[] floors;
    private FloorIterator floorIterator;

    public Dwelling(int numberOfFloors, Floor[] floors) {
        floorIterator = new FloorIterator(this);
        this.floors = new DwellingFloor[numberOfFloors];
        for (int i = 0; i < floors.length; i++) {
            this.floors[i] = floors[i];
        }
    }


    public Dwelling(Floor[] floors) {

        this.floors = floors;
    }

    @Override
    public Space getCurrentSpace(int index) {
        Space space;
        if (index < 0 || index >= getNumberOfSpaces()) {
            throw new SpaceIndexOutOfBoundsException("Invalid space number", index);
        } else {
            int temp = floors[0].getNumberOfSpaces();
            int i = 0;
            while (index > temp - 1) {
                index = index - temp;
                i++;
                temp = floors[i].getNumberOfSpaces();
            }
            space = floors[i].getSpace(index);
        }
        return space;
    }

    @Override
    public int getNumberOfFloors() {
        return floors.length;
    }

    @Override
    public int getNumberOfSpaces() {
        int number = 0;
        for (Floor floor : floors) {
            number += floor.getNumberOfSpaces();
        }
        return number;
    }

    @Override
    public double getArea() {
        double temp = 0;
        for (Floor floor : floors) {
            temp += floor.getArea();
        }
        return temp;
    }

    @Override
    public int getNumberOfRooms() {
        int temp = 0;
        for (Floor floor : floors) {
            temp += floor.getNumberOfRooms();
        }
        return temp;
    }

    @Override
    public Floor[] getFloorsArray() {
        return this.floors;
    }

    @Override
    public Floor getFloor(int index) {
        if (index < 0 || index > getNumberOfFloors()) {
            throw new FloorIndexOutOfBoundsException("Invalid Floor", index);
        } else {
            return floors[index];
        }
    }

    @Override
    public void setFloor(int index, Floor floor) {
        if (index < 0 || index > getNumberOfFloors()) {
            throw new FloorIndexOutOfBoundsException("Invalid Floor", index);
        } else {
            floors[index] = floor;
        }
    }

    @Override
    public Space getBestSpace() {
        Space temp;
        temp = floors[0].getBestSpace();
        for (int i = 1; i < floors.length; i++) {
            if (floors[i].getBestSpace().getArea() > temp.getArea()) {
                temp = floors[i].getBestSpace();
            }
        }
        return temp;
    }

    @Override
    public void setSpace(int index, Space flat) {
        if (index < 0 || index >= getNumberOfSpaces()) {
            throw new SpaceIndexOutOfBoundsException("Invalid flat number", index);
        } else {
            int temp;
            for (Floor floor : floors) {
                temp = floor.getNumberOfSpaces();
                if (index > temp - 1) {
                    index = index - temp;
                } else {
                    floor.setSpace(index, flat);
                    break;
                }
            }
        }
    }

    @Override
    public void addNewSpace(int index, Space flat) {
        if (index < 0 || index > getNumberOfSpaces()) {
            throw new SpaceIndexOutOfBoundsException("Invalid flat number", index);
        } else {
            int temp;
            for (Floor floor : floors) {
                temp = floor.getNumberOfSpaces();
                if (index <= temp) {
                    floor.addNewSpace(index, flat);
                    break;
                } else {
                    index = index - (temp - 1);
                }
            }
        }
    }

    @Override
    public void deleteSpace(int index) {
        if (index < 0 || index > getNumberOfSpaces()) {
            throw new SpaceIndexOutOfBoundsException("Invalid flat number", index);
        } else {
            int temp;
            for (Floor floor : floors) {
                temp = floor.getNumberOfSpaces();
                if (index > temp - 1) {
                    index = index - (temp - 1);
                } else {
                    floor.deleteSpace(index);
                    break;
                }
            }
        }
    }

    @Override
    public Space[] getSortArray() {
        Space[] temps = new Space[getNumberOfSpaces()];
        int index = 0;
        for (Floor floor : floors) {
            for (int j = 0; j < floor.getNumberOfSpaces(); j++) {
                temps[index] = floor.getSpace(j);
                index++;
            }
        }
        Space temp;
        for (int i = 0; i < temps.length; i++) {
            for (int j = 0; j + 1 < temps.length; j++) {
                if (temps[j].getArea() > temps[j + 1].getArea()) {
                    temp = temps[j];
                    temps[j] = temps[j + 1];
                    temps[j + 1] = temp;
                }
            }
        }
        return temps;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Dwelling)) return false;
        if (getNumberOfFloors() == ((Dwelling) obj).getNumberOfFloors()) {
            int index = 0;
            while (index < getNumberOfFloors()) {
                if (!PlacementExchanger.isExchangeFloor(getFloor(index), ((Dwelling) obj).getFloor(index))) {
                    return false;
                }
                index++;
            }
        } else return false;
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Dwelling dwelling = (Dwelling) super.clone();
        dwelling.floors = this.floors.clone();
        for (int i = 0; i < floors.length; i++) {
            dwelling.floors[i] = (Floor) this.floors[i].clone();
        }
        return dwelling;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Dwelling (2");
        for (int i = 0; i < getNumberOfFloors(); i++) {
            stringBuffer.append(", ");
            stringBuffer.append(getFloor(i));
        }
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfFloors();
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            hash = hash ^ this.getFloor(i).hashCode();
        }
        return hash;
    }

    @Override
    public Iterator iterator() {
        return null;
    }
}
