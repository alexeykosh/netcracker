package netCracker.buildings.Dwellings;

import netCracker.Floor;
import netCracker.PlacementExchanger;
import netCracker.Space;
import netCracker.SpaceIterator;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Created by Aleksey on 04.10.2016.
 */
public class DwellingFloor implements Floor, Serializable, Iterable<Space> {
    private Space[] flats;

    public DwellingFloor(int numberOfFlats) {
        if (numberOfFlats > 0) {
            flats = new Flat[numberOfFlats];
            for (int i = 0; i < flats.length; i++) {
                flats[i] = new Flat();
            }
        }
    }

    public DwellingFloor(Space[] space) {
        if (space.length > 0) {
            this.flats = new Space[space.length];
            for (int i = 0; i < flats.length; i++) {
                this.flats[i] = space[i];
            }
        }
    }

    @Override

    public int getNumberOfSpaces() {
        return flats.length;
    }

    @Override
    public double getArea() {
        double square = 0;
        for (Space flat : flats) {
            square += flat.getArea();
        }
        return square;
    }

    @Override
    public int getNumberOfRooms() {
        int number = 0;
        for (Space flat : flats) {
            number += flat.getNumberOfRooms();
        }
        return number;
    }

    @Override
    public Space[] getArrayOfSpaces() {
        return flats;
    }

    @Override
    public Space getSpace(int numberOfFlat) {
        if (numberOfFlat < 0 || numberOfFlat > getNumberOfSpaces()) {
            throw new IndexOutOfBoundsException("Invalid flat number");
        } else {
            return flats[numberOfFlat];
        }
    }

    @Override
    public void setSpace(int numberOfFlat, Space space) {

        if (numberOfFlat < flats.length) {
            flats[numberOfFlat] = space;
        } else {
            throw new IndexOutOfBoundsException("Invalid flat number");
        }
    }

    @Override
    public void addNewSpace(int numberOfFlat, Space flat) {
        if (numberOfFlat <= flats.length) {
            Space[] newFlats = new Flat[flats.length + 1];
            for (int i = 0; i < numberOfFlat; i++) {
                newFlats[i] = flats[i];
            }
            newFlats[numberOfFlat] = flat;
            for (int i = numberOfFlat + 1; i < flats.length; i++) {
                newFlats[i] = flats[i];
            }
            this.flats = newFlats;
        }
    }

    @Override
    public void deleteSpace(int numberOfFlat) {
        if (numberOfFlat > flats.length || numberOfFlat < 0) {
            throw new IndexOutOfBoundsException("Invalid flat number");
        } else {
            Space[] newFlats = new Flat[flats.length - 1];
            for (int i = 0; i < numberOfFlat; i++) {
                newFlats[i] = this.flats[i];
            }
            for (int i = numberOfFlat; i < newFlats.length; i++) {
                newFlats[i] = this.flats[i + 1];
            }
            this.flats = newFlats;
        }
    }

    @Override
    public Space getBestSpace() {
        Space temp;
        temp = flats[0];
        for (int i = 1; i < flats.length; i++) {
            if (flats[i].getArea() > temp.getArea()) {
                temp = flats[i];
            }
        }
        return temp;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (Space flat : flats) {
            stringBuffer.append(", Flat (")
                    .append(flat.getNumberOfRooms())
                    .append(", ").append(flat.getArea())
                    .append(")");
        }
        return "Dwelling Floor (" + flats.length + stringBuffer + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof DwellingFloor)) return false;
        return PlacementExchanger.isExchangeFloor(this, (DwellingFloor) obj);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DwellingFloor dwellingFloor = (DwellingFloor) super.clone();
        dwellingFloor.flats = this.flats.clone();
        for (int i = 0; i < dwellingFloor.getNumberOfSpaces(); i++) {
            dwellingFloor.flats[i] = (Space) this.flats[i].clone();
        }
        return dwellingFloor;
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfSpaces();
        for (int i = 0; i < this.getNumberOfSpaces(); i++) {
            hash = hash ^ this.getSpace(i).hashCode();
        }
        return hash;
    }

    @Override
    public Iterator iterator() {
        return new SpaceIterator(this);
    }

    @Override
    public int compareTo(Floor o) {
        if (o.getNumberOfSpaces() > this.getNumberOfSpaces()) return 1;
        if (o.getNumberOfSpaces() == this.getNumberOfSpaces()) return 0;
        return -1;
    }
}
