package netCracker.buildings.net.server.sequental.sequental;

import netCracker.Building;
import netCracker.Buildings;
import netCracker.buildings.Dwellings.DwellingFactory;
import netCracker.buildings.Dwellings.Hotel.HotelFactory;
import netCracker.buildings.Offices.OfficeFactory;
import netCracker.buildings.net.BuildingUnderArrestException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by Alexey on 29.11.2016.
 */
public class BinaryServer {

    private static double costOfBuilding(Building building) throws BuildingUnderArrestException {
        if (new Random().nextInt(10) == 0) {
            throw new BuildingUnderArrestException();
        } else {
            switch (building.getClass().getSimpleName()) {
                case "OfficeBuilding":
                    return building.getArea() * 1500;
                case "Dwelling":
                    return building.getArea() * 1000;
                case "Hotel":
                    return building.getArea() * 2000;
                default:
                    return 0;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(51221);
        Socket socket;
        boolean arrested;
        DecimalFormat myFormatter = new DecimalFormat("$###,###.###");
        System.out.println("Сервер ждет подключения...");
        while (true) {
            socket = serverSocket.accept();
            System.out.println("Сервер установил соединение...");
            double price = 0;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            DataInputStream dataIn = new DataInputStream(socket.getInputStream());
            String buffer;
            Building building;

            while (!(buffer = dataIn.readUTF()).equals("stop")) {
                arrested = false;
                System.out.println("Получен тип здания: " + buffer);
                switch (buffer) {
                    case "Dwelling":
                        Buildings.setBuildingFactory(new DwellingFactory());
                        break;
                    case "OfficeBuilding":
                        Buildings.setBuildingFactory(new OfficeFactory());
                        break;
                    case "Hotel":
                        Buildings.setBuildingFactory(new HotelFactory());
                        break;
                }

                building = Buildings.inputBuilding(socket.getInputStream());
                System.out.println("Получено здание: " + building);
                try {
                    System.out.println("Идет оценка стоимости здания...");
                    price = costOfBuilding(building);
                } catch (BuildingUnderArrestException e) {
                    //e.printStackTrace();
                    bufferedWriter.write("Здание под арестом." + "\n");
                    bufferedWriter.flush();
                    System.out.println("Здание под арестом.");
                    arrested = true;
                }
                if (!arrested) {
                    bufferedWriter.write(price + "\n");
                    bufferedWriter.flush();
                    System.out.println("Отправлена стоимость здания: " + myFormatter.format(price));
                }
                System.out.println();
            }
        }
    }
}
