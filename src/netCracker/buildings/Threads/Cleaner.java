package netCracker.buildings.Threads;

import netCracker.Floor;

/**
 * Created by Home on 15.11.2016.
 */
public class Cleaner extends Thread {
    private Floor floor;

    public Cleaner(Floor floor) {
        this.floor = floor;
    }

    @Override
    public void run() {
        for (int i = 0; i < floor.getNumberOfSpaces(); i++) {
            System.out.println("«Cleaning room number " + i + " with total area " +
                    floor.getSpace(i).getArea() + " square meters».");
        }
    }
}
