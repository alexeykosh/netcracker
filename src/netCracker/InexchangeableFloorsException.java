package netCracker;

/**
 * Created by Aleksey on 18.10.2016.
 */
public class InexchangeableFloorsException extends Exception {
    private String message;

    public InexchangeableFloorsException() {
        this.message = "Invalid floors for exception";
    }

    public InexchangeableFloorsException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
