package netCracker;

import java.util.Iterator;

/**
 * Created by Alexey on 18.10.2016.
 */
public interface Floor extends Comparable<Floor>, Iterable<Space> {

    int getNumberOfSpaces();

    double getArea();

    int getNumberOfRooms();

    Space[] getArrayOfSpaces();

    Space getSpace(int index);

    void setSpace(int index, Space space);

    void addNewSpace(int index, Space space);

    void deleteSpace(int index);

    Space getBestSpace();

    Object clone() throws CloneNotSupportedException;

    Iterator iterator();

}
