package netCracker;

import java.util.Iterator;

/**
 * Created by Home on 22.11.2016.
 */
public class SynchronizedFloor implements Floor {
    Floor floor;

    public SynchronizedFloor(Floor floor) {
        this.floor = floor;
    }

    @Override
    public synchronized int getNumberOfSpaces() {
        return floor.getNumberOfSpaces();
    }

    @Override
    public synchronized double getArea() {
        return floor.getArea();
    }

    @Override
    public synchronized int getNumberOfRooms() {
        return floor.getNumberOfRooms();
    }

    @Override
    public synchronized Space[] getArrayOfSpaces() {
        return floor.getArrayOfSpaces();
    }

    @Override
    public synchronized Space getSpace(int index) {
        return floor.getSpace(index);
    }

    @Override
    public synchronized void setSpace(int index, Space space) {
        floor.setSpace(index, space);
    }

    @Override
    public synchronized void addNewSpace(int index, Space space) {
        floor.addNewSpace(index, space);
    }

    @Override
    public synchronized void deleteSpace(int index) {
        floor.deleteSpace(index);
    }

    @Override
    public synchronized Space getBestSpace() {
        return floor.getBestSpace();
    }

    @Override
    public Iterator iterator() {
        return floor.iterator();
    }

    @Override
    public int compareTo(Floor o) {
        return floor.compareTo(o);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return floor.clone();
    }
}
