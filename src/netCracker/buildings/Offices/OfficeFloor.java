package netCracker.buildings.Offices;

import netCracker.*;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Created by Aleksey on 13.10.2016.
 */
public class OfficeFloor implements Floor, Serializable, Iterable<Space> {
    SinglyLinkedList list;

    public OfficeFloor(int numberOfSpaces) {
        list = new SinglyLinkedList();
        if (numberOfSpaces > 0) {
            for (int i = 0; i < numberOfSpaces; i++) {
                list.add(i, new Office());
            }
        } else {
            throw new InvalidRoomsCountException();
        }

    }

    public OfficeFloor(Space[] offices) {
        if (offices.length > 0) {
            list = new SinglyLinkedList();
            for (int i = 0; i < offices.length; i++) {
                list.add(i, new Office(offices[i].getNumberOfRooms(), offices[i].getArea()));
            }
        } else {
            throw new InvalidRoomsCountException();
        }
    }

    @Override
    public int getNumberOfSpaces() {

        int count = 0;
        SingleNode head = list.getNode(-1);
        SingleNode temp = head;
        while (temp.next != head) {
            count++;
            temp = temp.next;
        }
        return count;
    }

    @Override
    public double getArea() {
        double area = 0;
        SingleNode head = list.getNode(-1);
        SingleNode temp = head.next;
        while (temp != head) {

            area += temp.office.getArea();
            temp = temp.next;
        }
        return area;
    }

    @Override
    public int getNumberOfRooms() {
        int count = 0;
        SingleNode head = list.getNode(-1);
        SingleNode temp = head.next;
        while (temp != head) {
            count += temp.office.getNumberOfRooms();
            temp = temp.next;
        }
        return count;
    }

    @Override
    public Space[] getArrayOfSpaces() {
        Space[] offices = new Space[getNumberOfSpaces()];
        for (int i = 0; i < offices.length; i++) {
            offices[i] = (list.getNode(i)).office;
        }
        return offices;
    }

    @Override
    public Space getSpace(int index) {
        if (index < 0 || index > getNumberOfSpaces() - 1) {
            throw new SpaceIndexOutOfBoundsException("Invalid Office number", index);
        } else return list.getNode(index).office;
    }

    @Override
    public void setSpace(int index, Space space) {
        if (index < 0 || index > getNumberOfSpaces()) {
            throw new IndexOutOfBoundsException("Invalid office number");
        } else {
            list.getNode(index).office = space;
        }
    }

    @Override
    public void deleteSpace(int index) {
        if (index < 0 || index > getNumberOfSpaces()) {
            throw new IndexOutOfBoundsException();
        } else {
            list.delete(index);
        }
    }

    @Override
    public void addNewSpace(int index, Space space) {
        if (index < 0 || index > getNumberOfSpaces()) {
            throw new IndexOutOfBoundsException("invalid office number");
        } else {
            list.add(index, space);
        }
    }

    @Override
    public Space getBestSpace() {
        Space office = getSpace(0);
        SingleNode head = list.getNode(-1);
        SingleNode temp = head.next;
        while (temp != head) {
            if (temp.office.getArea() > office.getArea()) {
                office = temp.office;
            }
            temp = temp.next;
        }
        return office;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < getNumberOfSpaces(); i++) {
            stringBuffer.append(", Office (")
                    .append(getSpace(i).getNumberOfRooms())
                    .append(", ")
                    .append(getSpace(i).getArea())
                    .append(")");
        }
        return "Office Floor (" + getNumberOfSpaces() + stringBuffer + ")";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        OfficeFloor officeFloor = (OfficeFloor) super.clone();
        officeFloor.list = (SinglyLinkedList) list.clone();
        return officeFloor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(obj instanceof OfficeFloor)) return false;
        return PlacementExchanger.isExchangeFloor(this, (OfficeFloor) obj);
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfSpaces();
        for (int i = 0; i < this.getNumberOfSpaces(); i++) {
            hash = hash ^ this.getSpace(i).hashCode();
        }
        return hash;
    }

    @Override
    public Iterator iterator() {
        return new SpaceIterator(this);
    }

    @Override
    public int compareTo(Floor o) {
        if (o.getNumberOfSpaces() > this.getNumberOfSpaces()) return 1;
        if (o.getNumberOfSpaces() == this.getNumberOfSpaces()) return 0;
        return -1;
    }
}
