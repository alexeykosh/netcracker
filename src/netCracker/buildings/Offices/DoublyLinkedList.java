package netCracker.buildings.Offices;

import netCracker.Floor;

import java.io.Serializable;

/**
 * Created by Aleksey on 17.10.2016.
 */
public class DoublyLinkedList implements Serializable {
    public DoubleNode head;

    public DoublyLinkedList() {
        head = new DoubleNode();
        head.next = head;
        head.prev = head;
    }

    public DoubleNode getNode(int index) {
        DoubleNode temp = head;
        int i = -1;
        while (i != index) {
            if (temp.next != head) {
                temp = temp.next;
                i++;
            } else {
                throw new IndexOutOfBoundsException("Invalid number");
            }
        }
        return temp;
    }

    public void add(int index, Floor floor) {
        DoubleNode myOff = new DoubleNode(floor);
        if (index == 0) {
            if (head.next == head) {
                head.next = myOff;
                myOff.next = head;
                myOff.prev = head;
                head.prev = myOff;
            } else {
                myOff.next = head.next;
                head.next = myOff;
                myOff.prev = head;
                myOff.next.prev = myOff;
            }
        } else {
            DoubleNode temp = head;
            int i = -1;
            while (i != index - 1) {
                if (temp.next != head) {
                    temp = temp.next;
                    i++;
                } else {
                    throw new IndexOutOfBoundsException("Invalid index");
                }
            }
            if (temp.next == head) {
                myOff.next = temp.next;
                temp.next = myOff;
                myOff.prev = temp;
                head.prev = myOff;
            } else {
                myOff.next = temp.next;
                myOff.next.prev = myOff;
                temp.next = myOff;
                myOff.prev = temp;
            }
        }
    }

    public void delete(int index) {
        if (index == 0) {
            if (head.next == head) {
                throw new IndexOutOfBoundsException("List is empty");
            } else {
                DoubleNode temp = head.next;
                if (temp.next == head) {
                    head.next = head;
                    head.prev = head;
                    temp.next = null;
                    temp.prev = null;
                } else {
                    head.next = temp.next;
                    temp.next.prev = head;
                    temp.prev = null;
                    temp.next = null;
                }
            }
        } else {
            DoubleNode temp = head;
            DoubleNode temp1;
            int i = -1;
            while (i != index - 1) {
                temp = temp.next;
                i++;
            }
            temp1 = temp.next;
            if (temp1 == head) {
                throw new IndexOutOfBoundsException("Invalid argument");
            } else {
                if (temp1.next == head) {
                    temp1.next = null;
                    temp1.prev = null;
                    temp.next = head;
                    head.prev = temp;
                } else {
                    temp.next = temp1.next;
                    temp1.next = null;
                    temp1.prev = null;
                    temp.next.prev = temp;
                }
            }
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        DoublyLinkedList list = (DoublyLinkedList) super.clone();
        list.head = new DoubleNode();
        DoubleNode temp1 = this.head;
        DoubleNode temp2 = list.head;
        while (temp1.next != this.head) {
            temp1 = temp1.next;
            DoubleNode temp = new DoubleNode((Floor) temp1.floor.clone());
            temp2.next = temp;
            temp2 = temp2.next;
            temp2.next = list.head;
        }
        return list;
    }

}
