package netCracker;

/**
 * Created by Aleksey on 10.10.2016.
 */
public class FloorIndexOutOfBoundsException extends IndexOutOfBoundsException {
    private String message;

    public FloorIndexOutOfBoundsException() {
        message = "Wrong Floor number";
    }

    public FloorIndexOutOfBoundsException(String message) {
        this.message = message;
    }

    public FloorIndexOutOfBoundsException(String message, int number) {
        this.message = message + number;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
