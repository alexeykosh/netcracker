package netCracker;

/**
 * Created by Home on 15.11.2016.
 */
public interface BuildingFactory {

    Space createSpace(double area);

    Space createSpace(int roomsCount, double area);

    Floor createFloor(int spacesCount);

    Floor createFloor(Space[] spaces);

    Building createBuilding(int floorsCount, Floor[] spacesCounts);

    Building createBuilding(Floor[] floors);
}
