package netCracker;

/**
 * Created by Aleksey on 18.10.2016.
 */
public class PlacementExchanger {
    public static boolean isExchangeSpace(Space firstSpace, Space secondSpace) {
        return firstSpace.getArea() == secondSpace.getArea() && firstSpace.getNumberOfRooms() == secondSpace.getNumberOfRooms();
    }

    public static boolean isExchangeFloor(Floor firstFloor, Floor secondFloor) {
        return firstFloor.getArea() == secondFloor.getArea() && firstFloor.getNumberOfSpaces() == secondFloor.getNumberOfSpaces();
    }

    public static void exchangeFloorRooms(Floor floor1, int index1, Floor floor2, int index2) throws InexchangeableSpacesException {
        if (isExchangeSpace(floor1.getSpace(index1), floor2.getSpace(index2))) {
            floor1.setSpace(index1, floor2.getSpace(index2));
            floor2.setSpace(index2, floor1.getSpace(index1));
        } else throw new InexchangeableSpacesException();
    }

    public static void exchangeBuildingFloors(Building building1, int index1, Building building2, int index2) throws InexchangeableFloorsException {
        if (isExchangeFloor(building1.getFloor(index1), building2.getFloor(index2))) {
            building1.setFloor(index1, building2.getFloor(index2));
            building2.setFloor(index2, building1.getFloor(index1));
        } else throw new InexchangeableFloorsException();
    }

}
