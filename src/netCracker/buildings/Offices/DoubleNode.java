package netCracker.buildings.Offices;

import netCracker.Floor;

import java.io.Serializable;

/**
 * Created by Aleksey on 13.10.2016.
 */
public class DoubleNode implements Serializable {
    Floor floor;
    DoubleNode next;
    DoubleNode prev;

    public DoubleNode() {
        floor = null;
        next = null;
        prev = null;
    }

    public DoubleNode(Floor floor) {
        this.floor = floor;
        next = null;
        prev = null;
    }

}
