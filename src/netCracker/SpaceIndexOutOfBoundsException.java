package netCracker;

/**
 * Created by Aleksey on 10.10.2016.
 */
public class SpaceIndexOutOfBoundsException extends IndexOutOfBoundsException {
    private String message;

    public SpaceIndexOutOfBoundsException() {
        message = "Wrong space number";
    }

    public SpaceIndexOutOfBoundsException(String message, int number) {
        this.message = message + ": " + number;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
