package netCracker.buildings.Dwellings.Hotel;

import netCracker.Floor;
import netCracker.PlacementExchanger;
import netCracker.Space;
import netCracker.buildings.Dwellings.Dwelling;

/**
 * Created by Alexey on 24.10.2016.
 */

public class Hotel extends Dwelling {
    private String stars;

    public Hotel(int numberOfFloors, Floor[] floors) {
        super(numberOfFloors, floors);
    }

    public Hotel(Floor[] floors) {
        super(floors);
    }

    public String getStars() {
        Floor[] floors = super.getFloorsArray();
        String stars = "*";
        for (Floor floor : floors) {
            if (((HotelFloor) floor).getStars().length() > stars.length()) {
                stars = ((HotelFloor) floor).getStars();
            }
        }
        return stars;
    }

    @Override
    public Space getBestSpace() {
        Space[] spaces = super.getSortArray();
        Space temp = spaces[0];
        double rating = spaces[0].getArea() * 0.25;
        for (Space space : spaces) {
            switch (((Hotel) space).getStars().length()) {
                case 1:
                    if (space.getArea() * 0.25 > rating) {
                        temp = space;
                        rating = space.getArea() * 0.25;
                    }
                    break;
                case 2:
                    if (space.getArea() * 0.25 > rating) {
                        temp = space;
                        rating = space.getArea() * 0.5;
                    }
                    break;
                case 3:
                    if (space.getArea() * 0.25 > rating) {
                        temp = space;
                        rating = space.getArea() * 1;
                    }
                    break;
                case 4:
                    if (space.getArea() * 0.25 > rating) {
                        temp = space;
                        rating = space.getArea() * 1.25;
                    }
                    break;
                case 5:
                    if (space.getArea() * 0.25 > rating) {
                        temp = space;
                        rating = space.getArea() * 1.5;
                    }
                    break;
            }
        }
        return temp;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Hotel)) return false;
        if (getNumberOfFloors() == ((Hotel) obj).getNumberOfFloors()) {
            int index = 0;
            while (index < getNumberOfFloors()) {
                if (!PlacementExchanger.isExchangeFloor(getFloor(index), ((Hotel) obj).getFloor(index))) {
                    return false;
                }
                index++;
            }
        } else return false;
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); // вопрос!!!!!!!
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Hotel (2");
        for (int i = 0; i < getNumberOfFloors(); i++) {
            stringBuffer.append(", ");
            stringBuffer.append(getFloor(i));
        }
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfSpaces();
        for (int i = 0; i < this.getNumberOfSpaces(); i++) {
            hash = hash ^ this.getCurrentSpace(i).hashCode();
        }
        return hash;
    }
}
