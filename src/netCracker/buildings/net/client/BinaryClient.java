package netCracker.buildings.net.client;

import netCracker.Building;
import netCracker.Buildings;

import java.io.*;
import java.net.Socket;
import java.text.DecimalFormat;

/**
 * Created by Home on 17.11.2016.
 */
public class BinaryClient {
    public static void main(String[] args) throws IOException {

        Socket socket = new Socket("localhost", 51221);
        DecimalFormat myFormatter = new DecimalFormat("$###,###.###");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        FileWriter fileWriter = new FileWriter("3.txt");
        BufferedReader secondFile = new BufferedReader(new FileReader("2.txt"));
        FileReader fileReader = new FileReader("1.txt");
        String buffer;
        Building building;
        String cost;
        System.out.println("Соединение с сервером установлено");


        while ((buffer = secondFile.readLine()) != null) {

            dataOutputStream.writeUTF(buffer);
            System.out.println("Тип здания: " + buffer);
            building = Buildings.readBuilding(fileReader);
            System.out.println("Считано здание: " + building);
            Buildings.outputBuilding(building, socket.getOutputStream());
            System.out.println("Здание отправлено.");
            cost = bufferedReader.readLine();
            if (cost.equals("Здание под арестом.")) {
                System.out.println(cost);
                fileWriter.write(cost + "\n");
                fileWriter.flush();
            } else {
                cost = String.valueOf(myFormatter.format(Double.parseDouble(cost)));
                System.out.println("Получена стоимость здания: " + cost);
                fileWriter.write(cost + "\n");
                fileWriter.flush();
            }
            System.out.println();


        }
        System.out.println("Передача окончена");
        dataOutputStream.writeUTF("stop");

    }
}
