package netCracker;

/**
 * Created by Aleksey on 18.10.2016.
 */
public class InexchangeableSpacesException extends Exception {
    private String message;

    public InexchangeableSpacesException(String message) {
        this.message = message;
    }

    public InexchangeableSpacesException() {
        this.message = "Invalid spaces for exchange";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
