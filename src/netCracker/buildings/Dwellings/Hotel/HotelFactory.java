package netCracker.buildings.Dwellings.Hotel;

import netCracker.Building;
import netCracker.BuildingFactory;
import netCracker.Floor;
import netCracker.Space;
import netCracker.buildings.Dwellings.Flat;

/**
 * Created by Home on 15.11.2016.
 */
public class HotelFactory implements BuildingFactory {

    @Override
    public Space createSpace(double area) {
        return new Flat(area);
    }

    @Override
    public Space createSpace(int roomsCount, double area) {
        return new Flat(roomsCount, area);
    }

    @Override
    public Floor createFloor(int spacesCount) {
        return new HotelFloor(spacesCount);
    }

    @Override
    public Floor createFloor(Space[] spaces) {
        return new HotelFloor(spaces);
    }

    @Override
    public Building createBuilding(int floorsCount, Floor[] spacesCounts) {
        return new Hotel(floorsCount, spacesCounts);
    }

    @Override
    public Building createBuilding(Floor[] floors) {
        return new Hotel(floors);
    }
}
