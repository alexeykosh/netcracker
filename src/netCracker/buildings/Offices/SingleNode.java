package netCracker.buildings.Offices;

import netCracker.Space;

import java.io.Serializable;

/**
 * Created by Aleksey on 09.10.2016.
 */
public class SingleNode implements Serializable {
    public Space office;
    public SingleNode next;

    public SingleNode() {
        this.office = null;
        this.next = null;
    }

    public SingleNode(Space office) {
        this.next = null;
        this.office = office;
    }
}
