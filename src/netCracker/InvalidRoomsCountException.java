package netCracker;

/**
 * Created by Aleksey on 10.10.2016.
 */
public class InvalidRoomsCountException extends IllegalArgumentException {
    private String message;

    public InvalidRoomsCountException() {
        message = "Invalid number flats";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
