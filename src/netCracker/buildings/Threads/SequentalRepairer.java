package netCracker.buildings.Threads;

import netCracker.Floor;

/**
 * Created by Alexey on 28.11.2016.
 */
public class SequentalRepairer implements Runnable {
    private Floor floor;
    private Semaphore semaphore;

    public SequentalRepairer(Floor floor, Semaphore semaphore) {
        this.floor = floor;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        for (int i = 0; i < floor.getNumberOfSpaces(); i++) {
            try {
                semaphore.acquire();
                System.out.println("Repairing space number " + i + " with total area " +
                        floor.getSpace(i).getArea() + " square meters.");
                semaphore.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
