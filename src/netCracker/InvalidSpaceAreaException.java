package netCracker;

/**
 * Created by Aleksey on 10.10.2016.
 */
public class InvalidSpaceAreaException extends IllegalArgumentException {
    private String message;

    public InvalidSpaceAreaException() {
        message = "Incorrect area";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
