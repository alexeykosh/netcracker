package netCracker;

import java.util.Comparator;

/**
 * Created by Alexey on 22.11.2016.
 */
public class SpaceComparator implements Comparator<Space> {
    @Override
    public int compare(Space o1, Space o2) {
        return o1.getNumberOfRooms() - o2.getNumberOfRooms();
        //если вернул отриц число, то О1 больше т.к в нем меньше комнат
    }
}
