package netCracker.buildings.Offices;

import netCracker.*;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Created by Aleksey on 17.10.2016.
 */
public class OfficeBuilding implements Building, Serializable, Iterable {
    DoublyLinkedList list;
    FloorIterator floorIterator;

    public OfficeBuilding(Floor... floors) {
        if (floors.length == 0) {
            throw new FloorIndexOutOfBoundsException("Lenght = 0");
        } else {
            floorIterator = new FloorIterator(this);
            list = new DoublyLinkedList();
            for (int i = 0; i < floors.length; i++) {
                list.add(i, floors[i]);
            }
        }
    }

    public OfficeBuilding(int numberOfFloors, Floor[] floors) {
        if (numberOfFloors == 0) {
            throw new FloorIndexOutOfBoundsException("Lenght = 0");
        } else {
            floorIterator = new FloorIterator(this);
            list = new DoublyLinkedList();
            for (int i = 0; i < numberOfFloors; i++) {
                list.add(i, floors[i]);
            }
        }
    }

    @Override
    public int getNumberOfFloors() {
        DoubleNode head = list.getNode(-1);
        DoubleNode temp = head;
        int count = 0;
        while (temp.next != head) {
            count++;
            temp = temp.next;
        }
        return count;
    }

    @Override
    public int getNumberOfSpaces() {
        DoubleNode head = list.getNode(-1);
        DoubleNode temp = head.next;
        int count = 0;
        while (temp != head) {
            count += temp.floor.getNumberOfSpaces();
            temp = temp.next;
        }
        return count;
    }

    @Override
    public double getArea() {
        double area = 0;
        DoubleNode head = list.getNode(-1);
        DoubleNode temp = head.next;
        while (temp != head) {
            area += temp.floor.getArea();
            temp = temp.next;
        }
        return area;
    }

    @Override
    public int getNumberOfRooms() {
        int count = 0;
        DoubleNode head = list.getNode(-1);
        DoubleNode temp = head.next;
        while (temp != head) {
            count += temp.floor.getNumberOfRooms();
            temp = temp.next;
        }
        return count;
    }

    @Override
    public Floor[] getFloorsArray() {
        int count = this.getNumberOfFloors();
        Floor[] offices = new Floor[count];
        if (count == 0) {
            throw new IndexOutOfBoundsException("Список пуст!\n");
        } else {
            DoubleNode head = list.getNode(-1);
            DoubleNode temp = head.next;
            int i = 0;
            while (temp != head) {
                offices[i] = temp.floor;
                i++;
                temp = temp.next;
            }
            return offices;
        }
    }

    @Override
    public Floor getFloor(int index) {
        return list.getNode(index).floor;
    }

    @Override
    public void setFloor(int index, Floor floor) {
        list.getNode(index).floor = floor;
    }

    @Override
    public Space getCurrentSpace(int index) {
        int i;
        for (i = 0; i < getNumberOfFloors(); i++) {
            if (index <= list.getNode(i).floor.getNumberOfSpaces()) {
                break;
            } else {
                index = index - list.getNode(i).floor.getNumberOfSpaces();
            }
        }
        return list.getNode(index).floor.getSpace(index);
    }

    @Override
    public void setSpace(int index, Space office) {
        for (int i = 0; i < getNumberOfFloors(); i++) {
            if (index <= list.getNode(i).floor.getNumberOfSpaces()) {
                list.getNode(i).floor.setSpace(index, office);
                return;
            } else {
                index = index - list.getNode(i).floor.getNumberOfSpaces();
            }
        }
    }

    @Override
    public void addNewSpace(int index, Space office) {
        for (int i = 0; i < getNumberOfFloors(); i++) {
            if (index <= list.getNode(i).floor.getNumberOfSpaces()) {
                list.getNode(i).floor.addNewSpace(index, office);
                return;
            } else {
                index = index - list.getNode(i).floor.getNumberOfSpaces();
            }
        }
    }

    @Override
    public void deleteSpace(int index) {
        for (int i = 0; i < getNumberOfFloors(); i++) {
            if (index <= list.getNode(i).floor.getNumberOfSpaces()) {
                list.getNode(i).floor.deleteSpace(index);
                return;
            } else {
                index = index - list.getNode(i).floor.getNumberOfSpaces();
            }
        }
    }

    @Override
    public Space getBestSpace() {
        Space temp = list.getNode(0).floor.getBestSpace();
        if (list.getNode(0).floor != null) {
            for (int i = 0; i < getNumberOfFloors(); i++) {
                if (list.getNode(i).floor.getBestSpace().getArea() > temp.getArea()) {
                    temp = list.getNode(i).floor.getBestSpace();
                }
            }
        } else throw new FloorIndexOutOfBoundsException("List is empty");
        return temp;
    }

    @Override
    public Space[] getSortArray() {
        int count = 0;
        for (int i = 0; i < getNumberOfFloors(); i++) {
            count = count + list.getNode(i).floor.getNumberOfSpaces();
        }
        Space[] temp = new Space[count];
        int index1 = 0;
        for (int i = 0; i < getNumberOfFloors(); i++) {
            for (int j = 0; j < list.getNode(i).floor.getNumberOfSpaces(); j++) {
                temp[index1] = list.getNode(i).floor.getSpace(j);
                index1++;
            }
        }
        for (int i = 0; i < temp.length; i++) {
            for (int j = 0; j + 1 < temp.length; j++) {
                if (temp[j].getArea() < temp[j + 1].getArea()) {
                    Space buff = temp[j];
                    temp[j] = temp[j + 1];
                    temp[j + 1] = buff;
                }
            }
        }
        return temp;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        OfficeBuilding building = (OfficeBuilding) super.clone();
        building.list = (DoublyLinkedList) this.list.clone();
        return building;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Office Building (2");
        for (int i = 0; i < getNumberOfFloors(); i++) {
            stringBuffer.append(", ");
            stringBuffer.append(getFloor(i));
        }
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (this == null) return false;
        if (!(obj instanceof OfficeBuilding)) return false;
        if (getNumberOfFloors() == ((OfficeBuilding) obj).getNumberOfFloors()) {
            int index = 0;
            while (index < getNumberOfFloors()) {
                if (!PlacementExchanger.isExchangeFloor(getFloor(index), ((OfficeBuilding) obj).getFloor(index))) {
                    return false;
                }
                index++;
            }
        } else return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = this.getNumberOfFloors();
        for (int i = 0; i < this.getNumberOfFloors(); i++) {
            hash = hash ^ this.getFloor(i).hashCode();
        }
        return hash;
    }

    @Override
    public Iterator iterator() {
        return floorIterator;
    }
}



