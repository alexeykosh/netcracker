package netCracker;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Home on 15.11.2016.
 */
public class SpaceIterator implements Iterator {
    private Floor floor;
    private int index;

    public SpaceIterator(Floor floor) {
        this.floor = floor;
        index = 0;
    }


    @Override
    public boolean hasNext() {
        return index < floor.getNumberOfSpaces();
    }

    @Override
    public Space next() {
        if (hasNext()) {
            index++;
            return floor.getSpace(index - 1);
        } else throw new NoSuchElementException();
    }

    @Override
    public void remove() {

    }
}
