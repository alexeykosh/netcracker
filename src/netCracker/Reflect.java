package netCracker;

import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Alexey on 05.12.2016.
 */
public class Reflect {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException, FileNotFoundException {
        Class c = Class.forName("netCracker.ReflectExample");
        Constructor[] constructors = c.getConstructors();
//        for (int i = 0; i < constructors.length; i++) {
//            System.out.println(constructors[i]);
//        }
//        Method[] methods = c.getMethods();
//        for (int i = 0; i < methods.length; i++) {
//            System.out.println(methods[i]);
//        }

        Method w = c.getMethod("print", new Class[]{Double.TYPE});
        Object b = w.invoke(null, new Object[]{2.52});
        System.out.println(b);
    }
}
