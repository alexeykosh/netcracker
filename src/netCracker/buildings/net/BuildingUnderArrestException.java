package netCracker.buildings.net;

/**
 * Created by Alexey on 29.11.2016.
 */
public class BuildingUnderArrestException extends Exception {
    private String message;

    public BuildingUnderArrestException() {
        message = "Building under arrest";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
