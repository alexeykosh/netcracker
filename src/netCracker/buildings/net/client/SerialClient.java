package netCracker.buildings.net.client;

import netCracker.Building;
import netCracker.Buildings;

import java.io.*;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Objects;

/**
 * Created by Alexey on 02.12.2016.
 */
public class SerialClient {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Socket socket = new Socket("localhost", 51221);
        DecimalFormat myFormatter = new DecimalFormat("$###,###.###");
        BufferedReader secondFile = new BufferedReader(new FileReader("2.txt"));
        FileReader fileReader = new FileReader("1.txt");
        String buffer;
        Building building;
        Object object;
        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        System.out.println("Соединение с сервером установлено");


        while ((buffer = secondFile.readLine()) != null) {
            objectOutputStream.writeUTF(buffer);
            objectOutputStream.flush();

            System.out.println("Тип здания: " + buffer);
            building = Buildings.readBuilding(fileReader);
            System.out.println("Считано здание: " + building);

            objectOutputStream.writeObject(building);
            objectOutputStream.flush();
            System.out.println("Здание отправлено.");

            object = objectInputStream.readObject();
            if (Objects.equals(object.getClass().getSimpleName(), "BuildingUnderArrestException")) {
                System.out.println("Здание под арестом!.");
            } else {
                System.out.println("Получена стоимость здания: " + myFormatter.format(Double.valueOf(String.valueOf(object))));
            }
            System.out.println();
        }
        System.out.println("Передача окончена");
        objectOutputStream.writeUTF("stop");
        objectOutputStream.flush();

    }
}
