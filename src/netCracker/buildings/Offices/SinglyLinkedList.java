package netCracker.buildings.Offices;

import netCracker.Space;

import java.io.Serializable;

/**
 * Created by Aleksey on 09.10.2016.
 */
public class SinglyLinkedList implements Serializable {
    private SingleNode head;

    public SinglyLinkedList() {
        head = new SingleNode();
        head.next = head;

    }

    public SingleNode getNode(int number) {
        SingleNode temp = head;
        int i = -1;
        while (i != number) {
            if (temp.next != head) {
                temp = temp.next;
                i++;

            } else {
                throw new IndexOutOfBoundsException("Wrong index");
            }
        }
        return temp;
    }

    public void add(int number, Space space) {

        SingleNode myOff = new SingleNode(space);
        if (number == 0) { // в начало и список = 0
            if (head.next == head) {
                head.next = myOff;
                myOff.next = head;
            } else { // не в начало
                myOff.next = head.next;
                head.next = myOff;
            }
        } else { // в середину
            SingleNode temp = head;
            int i = -1;
            while (i != number - 1) {
                temp = temp.next;
                i++;
            }
            if (temp.next == head) {// в конец
                temp.next = myOff;
                myOff.next = head;
            } else {
                myOff.next = temp.next;
                temp.next = myOff;
            }

        }
    }

    public void delete(int index) {

        if (index == 0) {
            if (head.next == head) {
                throw new IndexOutOfBoundsException("List is empty");
            } else {
                SingleNode temp = head.next;
                head.next = temp.next;
            }
        } else {
            SingleNode temp = head;
            SingleNode temp2;
            int i = -1;
            while (i != index - 1) {
                temp = temp.next;
                i++;
            }
            temp2 = temp.next;
            if (temp.next == head) {
                System.out.println("Wrong index");
            } else {
                temp.next = temp2.next;
                temp2.next = null;
            }
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        SinglyLinkedList list = (SinglyLinkedList) super.clone();
        SingleNode head = new SingleNode();
        list.head = head;
        SingleNode temp = this.head;
        SingleNode temp2 = list.head;
        while (temp.next != this.head) {
            temp = temp.next;
            SingleNode temp3 = new SingleNode((Space) temp.office.clone());
            temp2.next = temp3;
            temp2 = temp2.next;
            temp2.next = list.head;
        }
        return list;
    }
}
