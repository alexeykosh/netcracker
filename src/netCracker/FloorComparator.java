package netCracker;

import java.util.Comparator;

/**
 * Created by Alexey on 22.11.2016.
 */
public class FloorComparator implements Comparator<Floor> {
    @Override
    public int compare(Floor o1, Floor o2) {
        return (int) (o1.getArea() - o2.getArea());
    }//вернет отрицательное число, если о1 больше, чем о2 т.к площадь меньше
}
