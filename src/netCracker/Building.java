package netCracker;

import java.util.Iterator;

/**
 * Created by Aleksey on 18.10.2016.
 */
public interface Building {

    int getNumberOfFloors();

    int getNumberOfSpaces();

    double getArea();

    int getNumberOfRooms();

    Floor[] getFloorsArray();

    Floor getFloor(int index);

    void setFloor(int index, Floor floor);

    Space getCurrentSpace(int index);

    void setSpace(int index, Space space);

    void addNewSpace(int index, Space space);

    void deleteSpace(int index);

    Space getBestSpace();

    Space[] getSortArray();

    Object clone() throws CloneNotSupportedException;

    Iterator iterator();


}
