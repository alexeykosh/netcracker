package netCracker;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Home on 15.11.2016.
 */
public class FloorIterator implements Iterator, Serializable {
    private Building building;
    private int index;

    public FloorIterator(Building building) {
        this.building = building;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        return index < building.getNumberOfFloors();
    }

    @Override
    public Object next() {
        if (hasNext()) {
            index++;
            return building.getFloor(index - 1);
        } else throw new NoSuchElementException();
    }

    @Override
    public void remove() {

    }
}
