package netCracker.buildings.net.server.sequental.sequental;

import netCracker.Building;
import netCracker.Buildings;
import netCracker.buildings.Dwellings.DwellingFactory;
import netCracker.buildings.Dwellings.Hotel.HotelFactory;
import netCracker.buildings.Offices.OfficeFactory;
import netCracker.buildings.net.BuildingUnderArrestException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by Alexey on 02.12.2016.
 */
public class SerialServer {
    private static double costOfBuilding(Building building) throws BuildingUnderArrestException {
        if (new Random().nextInt(10) == 0) {
            throw new BuildingUnderArrestException();
        } else {
            switch (building.getClass().getSimpleName()) {
                case "OfficeBuilding":
                    return building.getArea() * 1500;
                case "Dwelling":
                    return building.getArea() * 1000;
                case "Hotel":
                    return building.getArea() * 2000;
                default:
                    return 0;
            }
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Socket socket;
        DecimalFormat myFormatter = new DecimalFormat("$###,###.###");
        System.out.println("Сервер ждет подключения...");
        ServerSocket serverSocket = new ServerSocket(51221);
        while (true) {
            socket = serverSocket.accept();
            double price = 0;
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            String buffer;
            Building building;
            System.out.println("Сервер установил соединение...");


            while (!(buffer = objectInputStream.readUTF()).equals("stop")) {
                System.out.println("Получен тип здания: " + buffer);
                switch (buffer) {
                    case "Dwelling":
                        Buildings.setBuildingFactory(new DwellingFactory());
                        break;
                    case "OfficeBuilding":
                        Buildings.setBuildingFactory(new OfficeFactory());
                        break;
                    case "Hotel":
                        Buildings.setBuildingFactory(new HotelFactory());
                        break;
                }

                building = (Building) objectInputStream.readObject();
                System.out.println("Получено здание: " + building);
                try {
                    System.out.println("Идет оценка стоимости здания...");
                    price = costOfBuilding(building);
                } catch (BuildingUnderArrestException e) {
                    objectOutputStream.writeObject(e);
                    objectOutputStream.flush();
                    System.out.println("Здание под арестом.");

                }
                objectOutputStream.writeObject(String.valueOf(price));
                objectOutputStream.flush();
                System.out.println("Отправлена стоимость здания: " + myFormatter.format(price));
                System.out.println();
            }
        }
    }
}
