package netCracker.buildings.Threads;

import netCracker.Floor;

/**
 * Created by Alexey on 28.11.2016.
 */
public class SequentalCleaner implements Runnable {
    private Floor floor;
    private Semaphore semaphore;

    public SequentalCleaner(Floor floor, Semaphore semaphore) {
        this.floor = floor;
        this.semaphore = semaphore;
    }


    @Override
    public void run() {
        for (int i = 0; i < floor.getNumberOfSpaces(); i++) {
            try {
                semaphore.acquire();
                semaphore.setLastThread(Thread.currentThread());
                System.out.println("Cleaning room number " + i + " with total area " +
                        floor.getSpace(i).getArea() + " square meters.");
                semaphore.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
