package netCracker;

import netCracker.buildings.Dwellings.DwellingFactory;
import netCracker.buildings.Offices.Office;
import netCracker.buildings.Offices.OfficeBuilding;
import netCracker.buildings.Offices.OfficeFloor;

import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by Alexey on 23.10.2016.
 */
public class Buildings {
    public static BuildingFactory buildingFactory = new DwellingFactory();

    public static void setBuildingFactory(BuildingFactory newBuildingFactory) {
        buildingFactory = newBuildingFactory;
    }

    public static Building createBuilding(Floor... floors) {
        return buildingFactory.createBuilding(floors);
    }

    public static <T> T createBuilding(Floor floors, Class<T> buildingClass) {
        T building = null;
        try {
            Constructor<T> buildingConstructor = buildingClass.getConstructor(Array.class);
            building = buildingConstructor.newInstance(floors);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            throw new IllegalArgumentException();
        } catch (InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return building;
    }

    public static Building createBuilding(int floorCount, Floor... floors) {
        return buildingFactory.createBuilding(floorCount, floors);
    }

    public static <T> T createBuilding(int floorCount, Floor floors, Class<T> buildingClass) {
        T building = null;
        try {
            Constructor<T> buildingConstructor = buildingClass.getConstructor(Integer.TYPE, Array.class);
            building = buildingConstructor.newInstance(floorCount, floors);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            throw new IllegalArgumentException();
        } catch (InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return building;
    }


    public static Floor createFloor(int spaceCount) {
        return buildingFactory.createFloor(spaceCount);
    }

    public static <T> T createFloor(int spaceCount, Class<T> floorClass) throws NoSuchMethodException {
        T floor = null;
        Constructor<T> constructor = floorClass.getConstructor(new Class[]{Integer.TYPE});
        try {
            floor = constructor.newInstance(new Object[]{spaceCount});
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return floor;
    }

    public static Floor createFloor(Space... spaces) {
        return buildingFactory.createFloor(spaces);
    }

    public static <T> T createFloor(Space[] spaces, Class<T> floorClass) throws NoSuchMethodException {
        T floor = null;
        Constructor<T> constructor = floorClass.getConstructor(Space[].class);
        try {
            floor = constructor.newInstance(new Object[]{spaces});
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return floor;
    }

    public static Space createSpace(double area) {
        return buildingFactory.createSpace(area);
    }

    public static Space createSpace(int spaceCount, double area) {
        return buildingFactory.createSpace(spaceCount, area);
    }

    public static <T> T createSpace(int spaceCount, double area, Class<T> spaceClass) throws InvocationTargetException, InstantiationException {
        T space = null;
        try {
            Constructor<T> constructors = spaceClass.getConstructor(new Class[]{Integer.TYPE, Double.TYPE});
            space = constructors.newInstance(new Object[]{spaceCount, area});
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException();
        }
        return space;
    }

    public static <T> T createSpace(double area, Class<T> spaceClass) throws InvocationTargetException, InstantiationException {
        T space = null;
        try {
            Constructor<T> constructors = spaceClass.getConstructor(new Class[]{Double.TYPE});
            System.out.println(constructors);
            space = constructors.newInstance(new Object[]{area});
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException();
        }
        return space;
    }

    public static <T extends Comparable<T>> void sort(T[] objects) {
        for (int i = 0; i < objects.length; i++) {
            for (int j = i + 1; j < objects.length; j++) {
                if (objects[i].compareTo(objects[j]) == 1) {
                    T buff = objects[j];
                    objects[j] = objects[i];
                    objects[i] = buff;
                }
            }
        }
    }

    public static <T> void sort(T[] objects, Comparator<T> comparator) {
        for (int i = 0; i < objects.length; i++) {
            for (int j = i + 1; j < objects.length; j++) {
                if (comparator.compare(objects[j], objects[i]) < 0) {
                    T buff = objects[j];
                    objects[j] = objects[i];
                    objects[i] = buff;
                }
            }
        }
    }

    public static <T> void sortSpace(T[] objects) {
        sort(objects, Comparator.comparingInt(o -> ((Space) o).getNumberOfRooms()));
    }

    public static void outputBuilding(Building building, OutputStream out) throws IOException {
        DataOutputStream dataOut = new DataOutputStream(out);
        dataOut.writeInt(building.getNumberOfFloors());
        for (int i = 0; i < building.getNumberOfFloors(); i++) {
            dataOut.writeInt(building.getFloor(i).getNumberOfSpaces());
            for (int j = 0; j < building.getFloor(i).getNumberOfSpaces(); j++) {
                dataOut.writeInt(building.getFloor(i).getSpace(j).getNumberOfRooms());
                dataOut.writeDouble(building.getFloor(i).getSpace(j).getArea());
            }
        }
        dataOut.flush();

    }

    public static Building inputBuilding(InputStream in) throws IOException {
        DataInputStream dataIn = new DataInputStream(in);
        Floor[] of = new Floor[dataIn.readInt()];
        for (int i = 0; i < of.length; i++) {
            of[i] = Buildings.createFloor(dataIn.readInt());
            for (int j = 0; j < of[i].getNumberOfSpaces(); j++) {
                of[i].setSpace(j, Buildings.createSpace(dataIn.readInt(), dataIn.readDouble()));
            }
        }
        return Buildings.createBuilding(of);
    }

//    public static <T> T inputBuilding(InputStream in, Class<T> buildingClass, Class<T> floorClass, Class<T> spaceClass)
//            throws IOException, NoSuchMethodException, InvocationTargetException, InstantiationException {
//        DataInputStream dataIn = new DataInputStream(in);
//        T[] ts = (T[]) new Object[dataIn.readInt()];
//        for (int i = 0; i < ts.length; i++) {
//            ts[i] = Buildings.createFloor(dataIn.readInt(),floorClass);
//            for (int j = 0; j < ((Floor)ts[i]).getNumberOfSpaces(); j++) {
//                ((Floor)ts[i]).setSpace(j,(Space)Buildings.createSpace(dataIn.readInt(),dataIn.readDouble(),spaceClass));
//            }
//        }
//        return Buildings.createBuilding(ts,buildingClass);
//
//
//    }

    public static void writeBuilding(Building building, Writer out) throws IOException {
        out.write(building.getNumberOfFloors() + " ");
        for (int i = 0; i < building.getNumberOfFloors(); i++) {
            out.write(building.getFloor(i).getNumberOfSpaces() + " ");
            for (int j = 0; j < building.getFloor(i).getNumberOfSpaces(); j++) {
                out.write(building.getFloor(i).getSpace(j).getNumberOfRooms() + " ");
                out.write(building.getFloor(i).getSpace(j).getArea() + " ");
            }
        }
        out.write("\n");
        out.flush();
    }

    public static Building readBuilding(Reader in) throws IOException {
        StreamTokenizer stream = new StreamTokenizer(in);
        Floor[] officeFloors = null;
        int rooms = 0;
        double area = 0;
        if (stream.nextToken() != StreamTokenizer.TT_EOF) {
            officeFloors = new Floor[(int) stream.nval];
        }
        for (int i = 0; i < officeFloors.length; i++) {
            if (stream.nextToken() != StreamTokenizer.TT_EOF) {
                officeFloors[i] = Buildings.createFloor((int) stream.nval);
                for (int j = 0; j < officeFloors[i].getNumberOfSpaces(); j++) {
                    if (stream.nextToken() != StreamTokenizer.TT_EOF) {
                        rooms = (int) stream.nval;
                    }
                    if (stream.nextToken() != StreamTokenizer.TT_EOF) {
                        area = stream.nval;
                    }
                    officeFloors[i].setSpace(j, Buildings.createSpace(rooms, area));
                }
            }
        }
        Building building = Buildings.createBuilding(officeFloors);
        return building;
    }

    public static void writeBuildingFormat(Building building, Writer out) {
        PrintWriter printWriter = new PrintWriter(out);
        printWriter.printf("%d", building.getNumberOfFloors());
        for (int i = 0; i < building.getNumberOfFloors(); i++) {
            printWriter.printf("% d", building.getFloor(i).getNumberOfSpaces());
            for (int j = 0; j < building.getFloor(i).getNumberOfSpaces(); j++) {
                printWriter.printf("% d", building.getFloor(i).getSpace(j).getNumberOfRooms());
                printWriter.printf(Locale.US, "% .2f", building.getFloor(i).getSpace(j).getArea());
            }
        }
        printWriter.close();
    }

    public static Building readBuilding(Scanner scanner) throws IOException {
        Floor[] officeFloors = new Floor[scanner.nextInt()];
        for (int i = 0; i < officeFloors.length; i++) {
            officeFloors[i] = new OfficeFloor(scanner.nextInt());
            for (int j = 0; j < officeFloors[i].getNumberOfSpaces(); j++) {
                officeFloors[i].setSpace(j, new Office(scanner.nextInt(), scanner.nextDouble()));
            }
        }
        Building building = new OfficeBuilding(officeFloors);
        return building;
    }


//    public static <T,E> E readBuilding (Scanner scanner,Class<E> buildingClass, Class<T> floorClass, Class<T> spaceClass)
//            throws IOException, NoSuchMethodException, InvocationTargetException, InstantiationException {
//        T[] officeFloors = (T[])new Floor[scanner.nextInt()];
//        for (int i = 0; i < officeFloors.length; i++) {
//            officeFloors[i] = Buildings.createFloor(scanner.nextInt(),floorClass);
//            for (int j = 0; j < ((Floor)officeFloors[i]).getNumberOfSpaces(); j++) {
//                ((Floor)officeFloors[i]).setSpace(j,(Space)createSpace(scanner.nextInt(),scanner.nextDouble(),spaceClass));
//            }
//        }
//        return createBuilding(officeFloors,buildingClass);
//    }


}
