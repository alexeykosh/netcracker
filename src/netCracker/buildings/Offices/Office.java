package netCracker.buildings.Offices;

import netCracker.InvalidRoomsCountException;
import netCracker.InvalidSpaceAreaException;
import netCracker.PlacementExchanger;
import netCracker.Space;

import java.io.Serializable;

/**
 * Created by Aleksey on 09.10.2016.
 */
public class Office implements Space, Serializable {
    private static final double SQUARE = 250;
    private static final int ROOMS = 1;

    private double square;
    private int numberOfRooms;

    public Office() {
        square = SQUARE;
        numberOfRooms = ROOMS;
    }

    public Office(int numberOfRooms, double square) {
        if (square < 0) throw new InvalidSpaceAreaException();
        else if (numberOfRooms < 0) throw new InvalidRoomsCountException();
        else {
            this.numberOfRooms = numberOfRooms;
            this.square = square;
        }
    }

    public Office(double square) {
        if (square < 0) throw new InvalidSpaceAreaException();
        else {
            this.square = square;
            numberOfRooms = ROOMS;
        }
    }

    @Override
    public void setArea(double square) {
        this.square = square;
    }

    @Override
    public void setRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    @Override
    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    @Override
    public double getArea() {
        return square;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(obj instanceof Office)) return false;
        return PlacementExchanger.isExchangeSpace(this, (Office) obj);
    }

    @Override
    public int hashCode() {
        int hash;
        Double area = getArea();
        long l = area.longValue();
        hash = numberOfRooms ^ ((int) l ^ (int) l >> 32);
        return hash;
    }

    @Override
    public int compareTo(Space o) {
        if (o.getArea() > this.getArea()) return -1;
        if (o.getArea() == this.getArea()) return 0;
        return 1;
    }

    @Override
    public String toString() {
        return "Office " + "(" + numberOfRooms + ", " + square + ")";
    }
}
