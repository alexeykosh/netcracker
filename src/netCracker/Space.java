package netCracker;

/**
 * Created by Aleksey on 10.10.2016.
 */
public interface Space extends Comparable<Space> {

    int getNumberOfRooms();

    double getArea();

    void setRooms(int numberOfRooms);

    void setArea(double area);

    Object clone() throws CloneNotSupportedException;
}
